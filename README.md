db-services-intro
=================

This is an introduction to the CERN IT Databases Group intended to be
covered in less than 10 minutes as part of the AI introductory courses. 

Files
=====

 * beamerthemeCERN.sty - The beamer theme definition (colors, fonts, etc)
 * db-services-intro.tex - Presentation code
 * db-services-intro.pdf - Presentation
 * logos/ - A folder with the CERN logos used on the template


